import React from 'react';
import App from './App';
import { shallow, configure, mount, render } from 'enzyme';
import SpacesNav from './components/spaces';
import SpaceDetails from './components/space-details';
import EntriesTable from './components/entries-table';
import Adapter from 'enzyme-adapter-react-16';
import { Tabs, MenuItem, Hidden, TableBody, TableRow, TableSortLabel } from 'material-ui'

// set up configs
configure({ adapter: new Adapter() });
global.requestAnimationFrame = function(callback) {
    setTimeout(callback, 0);
};

// UI Component Tests
let SpaceStore = {};
let UsersStore = {}
let EntriesStore = {}

describe('Space Component Tests', function () {

    beforeEach(function () {
        SpaceStore = {
            spaces: {
                total: 2,
                items: [
                    {fields: {title: 'Space 1'} ,sys: {id: '1'}},
                    {fields: {title: 'Space 2'} ,sys: {id: '2'}},
                ]
            },
            selectedSpace: '',
            spaceById(){
                return {
                    fields: { title: 'test' },
                    sys: {id: '1'}
                }
            },
            redirectToSpace: function(selected){
                this.selectedSpace = selected
            },
            fetchSpaces: function(){

            }
        };
        UsersStore = {
            fetchUsers: function(){

            }
        }
    })

    it('Should show all the spaces', function () {
        const spaceComponent = shallow(
            <SpacesNav.wrappedComponent spacesStore={SpaceStore}
                                        usersStore={UsersStore}
                                        classes={{ selected: ''}}
                                        location={{ pathname: '/spaceexplorer'}} />
        )
        expect(spaceComponent.find(Hidden).at(0).find(MenuItem).length).toBe(2)
    })

    it('Should redirect to space details when menu item clicked', function () {
        const spaceComponent = shallow(
            <SpacesNav.wrappedComponent spacesStore={SpaceStore}
                                        usersStore={UsersStore}
                                        classes={{ selected: ''}}
                                        location={{ pathname: '/spaceexplorer'}} />
        )
        spaceComponent.find(Hidden).at(0).find(MenuItem).at(1).simulate('click')
        expect(SpaceStore.selectedSpace).toEqual("2")
    })
})

describe('Space Details Tests', function () {

    beforeEach(function(){
        SpaceStore = {
            spaces: {
                total: 2,
                items: [
                    {sys: {id: '1'}},
                    {sys: {id: '2'}},
                ]
            },
            tabIndex: 0,
            spaceById: function(id){
                return {
                    fields: { title: 'test' },
                    sys: {id: '1'}
                }
            },
            updateTab: function(tab){
                this.tabIndex = tab
            }
        };
        UsersStore = {
            userById: function(){ }
        }
    })

    it('Should show entries table when selected', function () {
        const spaceDetails = shallow(<SpaceDetails spacesStore={ SpaceStore } usersStore={ UsersStore } match={{params: { spaceId: '1' }}} />);
        expect(spaceDetails.props().spacesStore.tabIndex).toEqual(0)
    })

    it('Should show assets table when selected', function () {
        const spaceDetails = shallow(<SpaceDetails.wrappedComponent spacesStore={ SpaceStore } usersStore={ UsersStore } match={{params: { spaceId: '1' }}} />);
        spaceDetails.find(Tabs).simulate('change', 'event', 1);
        expect(SpaceStore.tabIndex).toEqual(1)
    })
})

describe('Entries Table Tests', function () {

    beforeEach(function(){
        let entries = [
            { fields: { title: 'entry 1', description: 'test'}, sys: { id: 1, createdBy: '1', updatedBy: '1', updatedAt: '2017-12-11T11:29:46.809Z'}},
            { fields: { title: 'entry 2', description: 'test'}, sys: { id: 2, createdBy: '1', updatedBy: '1', updatedAt: '2017-12-11T11:29:46.809Z'}}]
        EntriesStore = {
            entries: {
                items: entries,
                total: 2
            },
            updateSort: function(search){ },
            sortedEntries: {
                items: entries,
                total: 2 },
            sort: {
                orderBy: '',
                direction: 'desc'
            },
            columns: [
                { id: 'fields.title', label: 'Title', direction: 'desc' },
                { id: 'fields.summary', label: 'Summary', direction: 'desc' }],
            loading: false
        };
        UsersStore = {
            userById: function(){ }
        }
    })

    it('Should list all rows for each entry', function () {
        const wrapper = shallow(<EntriesTable.wrappedComponent usersStore={ UsersStore } entriesStore={ EntriesStore } />);
        expect(wrapper.find(TableBody).find(TableRow).length).toEqual(2)
    })

    it('Should sort the list by clicking a table header', function () {
        const spy = jest.spyOn(EntriesStore, 'updateSort');
        const wrapper = shallow(<EntriesTable.wrappedComponent usersStore={ UsersStore } entriesStore={ EntriesStore } />);
        wrapper.find(TableSortLabel).at(0).simulate('click')
        expect(spy).toHaveBeenCalled()
    })
})

