import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { TextField, Tabs, Tab } from 'material-ui';
import EntriesTable from './entries-table';
import AssetsTable from './assets-table';

@observer(['spacesStore', 'usersStore'])
export default class SpaceDetails extends Component {

    getSpaceDetails(match){
        if(match){
            // grab space from spaceId prop
            let space = this.props.spacesStore.spaceById(match.params.spaceId);
            let { tabIndex } = this.props.spacesStore;
            return (
                space ?
                    <div className="space-container">
                        <h1>{ space.fields.title } <span className="space-creator">created by { this.props.usersStore.userById(space.sys.createdBy )}</span></h1>
                        <TextField id="Space-Description"
                                   value={ space.fields.description }
                                   multiline
                                   placeholder="Your space description..."
                                   className="space-description"
                                   rowsMax="4"
                                   margin="normal"
                        />
                        <Tabs className="space-tabs"
                              indicatorColor="primary"
                              onChange={(event, tabIndex) => {
                                  this.props.spacesStore.updateTab(tabIndex)
                              }}
                              value={ tabIndex }>
                            <Tab className="space-tab" label="Entries" />
                            <Tab className="space-tab" label="Assets" />
                        </Tabs>
                        { tabIndex === 0 && <EntriesTable spaceId={ match.params.spaceId } />  }
                        { tabIndex === 1 && <AssetsTable spaceId={ match.params.spaceId } />}
                    </div>
                    : <h1>Loading...</h1>
            )
        }
    }

    render(){
        let { match } = this.props;
        return  (
            this.getSpaceDetails(match)
        )

    }
}