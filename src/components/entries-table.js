import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableSortLabel,
    TableRow } from 'material-ui/Table';
import { observer } from 'mobx-react';
import moment from 'moment';
import { CircularProgress } from 'material-ui';
import { errorHandler } from '../Error';

@observer(['entriesStore', 'usersStore'])
export default class EntriesTable extends Component {

    getRowsForEntries(){
        let entries = this.props.entriesStore.sortedEntries;
        return entries.items ?
            entries.items.map((item) => (
                <TableRow key={ item.sys.id }>
                    <TableCell>{ item.fields.title }</TableCell>
                    <TableCell>{ item.fields.summary }</TableCell>
                    <TableCell>{ this.props.usersStore.userById(item.sys.createdBy) }</TableCell>
                    <TableCell>{ this.props.usersStore.userById(item.sys.updatedBy) }</TableCell>
                    <TableCell>{ moment(item.sys.updatedAt).format('M-DD-YYYY')}</TableCell>
                </TableRow>
            ))
            : null
    }

    sortHandler(column){
        this.props.entriesStore.updateSort({
            orderBy: column.id,
            direction: column.direction === 'desc' ? 'asc' : 'desc'
        })
    }

    getTableHeaders(){
        return this.props.entriesStore.columns.map(column =>
            <TableCell key={ column.id }>
                <TableSortLabel active={ this.props.entriesStore.sort.orderBy === column.id }
                                direction={ column.direction }
                                onClick={ () => this.sortHandler(column) }>
                    { column.label }
                </TableSortLabel>
            </TableCell>
        )
    }

    render(){
        return (
            <Paper className="space-table">
                {
                    this.props.entriesStore.loading ?
                        <div className="center-block">
                            <CircularProgress size={50} />
                        </div> :
                        (
                            this.props.entriesStore.entries.total ?
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            { this.getTableHeaders() }
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        { this.getRowsForEntries() }
                                    </TableBody>
                                </Table> :
                                errorHandler(this.props.entriesStore.error)
                        )
                }
            </Paper>
        )
    }
}