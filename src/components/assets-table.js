import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableSortLabel,
    TableRow } from 'material-ui/Table';
import { observer } from 'mobx-react';
import moment from 'moment';
import { CircularProgress } from 'material-ui';
import { errorHandler } from '../Error';

@observer(['assetsStore', 'usersStore'])
export default class AssetsTable extends Component {

    componentDidMount(){
        this.props.assetsStore.fetchAssets({ spaceId: this.props.spaceId})
    }

    getRowsForAssets(){
        let assets = this.props.assetsStore.sortedAssets;
        return assets.items ?
            assets.items.map((item) => (
                <TableRow key={ item.sys.id }>
                    <TableCell>{ item.fields.title }</TableCell>
                    <TableCell>{ item.fields.contentType }</TableCell>
                    <TableCell>{ item.fields.fileName }</TableCell>
                    <TableCell>{ this.props.usersStore.userById(item.sys.createdBy) }</TableCell>
                    <TableCell>{ this.props.usersStore.userById(item.sys.createdBy) }</TableCell>
                    <TableCell>{ moment(item.sys.updatedAt).format('M-DD-YYYY') }</TableCell>
                </TableRow>
            ))
            : null
    }

    sortHandler(column){
        this.props.assetsStore.updateSort({
            orderBy: column.id,
            direction: column.direction === 'desc' ? 'asc' : 'desc'
        })
    }

    getTableHeaders(){
        return this.props.assetsStore.columns.map(column =>
            <TableCell key={ column.id }>
                <TableSortLabel direction={ column.direction }
                                active={ this.props.assetsStore.sort.orderBy === column.id }
                                onClick={ () => this.sortHandler(column) }>
                    { column.label }
                </TableSortLabel>
            </TableCell>
        )
    }

    render(){
        return (
            <Paper className="space-table">
                {
                    this.props.assetsStore.loading ?
                        <div className="loading-container">
                            <CircularProgress size={50} />
                        </div> :
                        (
                            this.props.assetsStore.assets.total ?
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            { this.getTableHeaders() }
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        { this.getRowsForAssets() }
                                    </TableBody>
                                </Table> :
                                errorHandler(this.props.assetsStore.error)
                        )
                }
            </Paper>
        )
    }
}