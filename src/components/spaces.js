import React, { Component } from 'react';
import {
    MenuItem,
    MenuList,
    CircularProgress,
    Hidden,
    Drawer,
    Toolbar,
    IconButton,
    AppBar,
    Typography } from 'material-ui';
import  MenuIcon  from 'material-ui-icons/Menu';
import { observer } from 'mobx-react';
import { Route } from 'react-router-dom';
import SpaceDetails from './space-details';
import { withRouter, Redirect } from 'react-router';
import { withStyles } from 'material-ui/styles'

const styles = theme => ({
    selected: {
        background: '#0B3F9C',
        color: 'white',
        '&:focus': {
            background: '#0B3F9C',
            color: 'white',
        }
    }
})

@observer(['entriesStore', 'spacesStore', 'usersStore'])
class Spaces extends Component {

    constructor(){
        super();
        this.state = {
            mobileOpen: false
        }
    }

    componentDidMount(){
        this.props.spacesStore.fetchSpaces();
        this.props.usersStore.fetchUsers()
    }

    getSpaces(){
        let spaces = this.props.spacesStore.spaces.items;
        return spaces ? spaces.map((item) =>
            <MenuItem classes={{ selected: this.props.classes.selected }}
                      key={ item.sys.id }
                      onClick={() => {
                          this.props.spacesStore.redirectToSpace(item.sys.id);
                          if(this.state.mobileOpen) this.toggleDrawer();
                      }}
                      selected={ item.sys.id === this.props.spacesStore.selectedSpace }>
                { item.fields.title }
            </MenuItem>
        ) : <CircularProgress className="loading-centered"  size={50} />
    }
    toggleDrawer(){
        this.setState({
            mobileOpen: !this.state.mobileOpen
        })
    }
    redirectToSelected(){
        // If Current pathname is different then selectedSpaceId, navigate to it.
        let location = this.props.location.pathname.split('/');
        let { selectedSpace } = this.props.spacesStore;
        if(selectedSpace && (location[2] !== selectedSpace)){
            this.props.spacesStore.updateTab(0);
            this.props.entriesStore.fetchEntries({spaceId: selectedSpace});
            // return redirect tag to navigate to space-detail
            return ( <Redirect push={true} to={`/spaceexplorer/${selectedSpace}`} /> )
        }
    }

    render(){
        return (
            <div className="flex-container">
                <Hidden xsDown>
                    <MenuList className="menu-nav">
                        { this.getSpaces() }
                    </MenuList>
                </Hidden>
                <Hidden smUp>
                    <AppBar position="fixed">
                        <Toolbar>
                            <IconButton
                                color="contrast"
                                aria-label="open drawer"
                                onClick={() => this.toggleDrawer()}>
                                <MenuIcon />
                            </IconButton>
                            <Typography type="title" color="inherit">
                                Space Explorer
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <Drawer type="temporary"
                            onRequestClose={() => this.toggleDrawer()}
                            open={this.state.mobileOpen}>
                        <MenuList className="menu-nav">
                            { this.getSpaces() }
                        </MenuList>
                    </Drawer>
                </Hidden>
                <div className="content">
                    { this.redirectToSelected() }
                    <Route path="/spaceexplorer/:spaceId"
                           push="true"
                           component={ SpaceDetails } />
                </div>
            </div>
        )
    }
}
// use withStyles for access to material classes, withRouter for access to location object
const SpacesNav = withStyles(styles)(withRouter(Spaces));
export default SpacesNav