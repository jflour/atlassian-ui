import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { Provider } from 'mobx-react';
import stores from './stores/index'

//components
import SpacesNav from './components/spaces';

//styles
import './App.scss';

class App extends Component {
  render() {
    return (
        <Provider { ...stores }>
          <Router>
            <div>
              <Redirect from="/" to="/spaceexplorer" />
              <Route path="/spaceexplorer" component={ SpacesNav } />
            </div>
          </Router>
        </Provider>
    );
  }
}

export default App;
