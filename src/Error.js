import React from 'react';

export const errorHandler = (error) => {
    if(error){
        return (
            <div className="error-container">
                <div className="alert">
                    { error }
                </div>
            </div>
        )
    } else {
        return <div className="center-block">None found</div>
    }
}