import SpacesStore from './spaces';
import EntriesStore from './entries';
import AssetsStore from './assets';
import UsersStore from './users';

export default {
    spacesStore: new SpacesStore(),
    entriesStore: new EntriesStore(),
    assetsStore: new AssetsStore(),
    usersStore: new UsersStore()
}

