import { observable, action, computed } from 'mobx';
import _ from 'lodash';

export default class EntriesStore {
    @observable entries = {};
    @observable sort = {
        orderBy: '',
        direction: 'desc'
    }
    @observable error;
    @observable loading = true;
    @observable columns = [
        { id: 'fields.title', label: 'Title', direction: 'desc' },
        { id: 'fields.summary', label: 'Summary', direction: 'desc' },
        { id: 'sys.createdBy', label: 'Created By', direction: 'desc' },
        { id: 'sys.updatedBy', label: 'Updated By', direction: 'desc' },
        { id: 'sys.updatedAt', label: 'Last Updated', direction: 'desc' }
    ]

    @action fetchEntries(params){
        this.loading = true;
        fetch(`https://us-central1-space-explorer-a54c5.cloudfunctions.net/api/space/${params.spaceId}/entries`, {
            method: 'GET'
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.loading = false;
            this.error = responseJson.error || 0;
            this.entries = this.error ? {} : responseJson
        })
        .catch((error) => {
            this.error = error;
        })
    }

    @computed get sortedEntries(){
        if(!this.sort.orderBy) return this.entries;
        let sorter = this.sort.orderBy.split('.');
        let ordered = _.orderBy(
            this.entries.items,
            function(o){
                return o[sorter[0]][sorter[1]]
            },
            [this.sort.direction]
        )
        return {
            ...this.entries,
            items: ordered
        }
    }

    @action updateSort(sort){
        this.sort = sort;
        let index = _.findIndex(this.columns, o => o.id === sort.orderBy);
        this.columns[index].direction = this.sort.direction
    }
}
