import { observable, action, computed } from 'mobx';
import _ from 'lodash';

export default class AssetsStore {
    @observable assets = {};
    @observable sort = {
        orderBy: '',
        direction: 'desc'
    };
    @observable error;
    @observable loading = true;
    @observable columns = [
        { id: 'fields.title', label: 'Title', direction: 'desc' },
        { id: 'fields.contentType', label: 'Content', direction: 'desc' },
        { id: 'fields.fileName', label: 'File Name', direction: 'desc' },
        { id: 'sys.createdBy', label: 'Created By', direction: 'desc' },
        { id: 'sys.updatedBy', label: 'Updated By', direction: 'desc' },
        { id: 'sys.updatedAt', label: 'Last Updated', direction: 'desc' }
    ]

    @action fetchAssets(params){
        this.loading = true;
        fetch(`https://us-central1-space-explorer-a54c5.cloudfunctions.net/api/space/${params.spaceId}/assets`, {
            method: 'GET'
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.loading = false;
            this.error = responseJson.error || 0;
            this.assets = this.error ? {} : responseJson
        })
    }

    @computed get sortedAssets(){
        if(!this.sort.orderBy) return this.assets;
        let sorter = this.sort.orderBy.split('.');
        let ordered = _.orderBy(
            this.assets.items,
            function(o){
                return o[sorter[0]][sorter[1]]
            },
            [this.sort.direction]
        )
        return {
            ...this.assets,
            items: ordered
        }
    }

    @action updateSort(sort){
        this.sort = sort;
        let index = _.findIndex(this.columns, o => o.id === sort.orderBy);
        this.columns[index].direction = this.sort.direction
    }
}