import { observable, action } from 'mobx';

export default class SpacesStore {
    @observable spaces = {};
    @observable loading = true;
    @observable tabIndex = 0;
    @observable selectedSpace = '';

    @action fetchSpaces(){
        fetch('https://us-central1-space-explorer-a54c5.cloudfunctions.net/api/space', {
            method: 'GET'
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.loading = false;
            this.spaces = responseJson;
            if(!this.selectedSpace){
                this.selectedSpace = responseJson.items[0].sys.id
            }
        })
    }

    @action updateTab(tab) {
        this.tabIndex = tab;
    }

    @action redirectToSpace(id){
        this.selectedSpace = id;
    }

    spaceById(id){
        let { items } = this.spaces;
        if(!items) return null;
        let space = null;
        for(let i = 0; i < items.length; i++){
            if(items[i].sys.id === id){
                space = items[i]
                break;
            }
        }
        return space;
    }
}