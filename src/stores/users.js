import { observable, action } from 'mobx';
import _ from 'lodash';

export default class UsersStore {
    @observable users = {};

    @action fetchUsers(){
        fetch(`https://us-central1-space-explorer-a54c5.cloudfunctions.net/api/users`, {
            method: 'GET'
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.users = responseJson
        })
    }
     userById(id){
        let user =  _.find(this.users.items, o =>  o.sys.id === id)
         if(user){
            return user.fields.name
         }
     }
}