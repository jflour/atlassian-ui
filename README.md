## About

This exercise was created with React and mobX for state management. It uses a backend firebase server to mock out the backend api. The tests are written with Jest and Enzyme.

### `Running locally`

1. npm install 
2. npm start

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `Console errors`

1. the console error about trying to redirect to /spaceexplorer but already there, is a current issue on github: https://github.com/guyellis/learn/issues/205.
2. 500 error is from the mock api hosted on firebase to show the error handling on the frontend.
